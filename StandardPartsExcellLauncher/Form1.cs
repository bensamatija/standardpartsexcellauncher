﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Diagnostics;
using System.Threading;

namespace StandardPartsExcellLauncher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // PUBLIC VARS:
        string appName = "StandardPartsLauncher";
        string excellFileName = "StandardParts.xlsx";
        string excellLocalFileLocation = "C:\\StandardPartsExcel";
        string excellRemoteFileLocation = @"\\10.230.17.200\TSModels\ApplicationProgramFiles\StandardPartsExcel";
        //bool remoteFileNotFound = false;

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.Visible = false;
            txtAppName.Text = appName;
            this.Text = appName;
            lblTaskInfo.Text = "Synching data...";
            GUIrefresh();

            Program();
        }

        private void Program()
        {
            try
            {
                // Compare files:
                CompareFiles();

                // Launch the file:
                LaunchTheExcellFile();
            }
            catch (Exception e) { MessageBox.Show(e.ToString()); }
        }

        private void CompareFiles()
        {
            lblTaskInfo.Text = "Comparing the files with remote version...";
            GUIrefresh();

            bool filesAreTheSame = false;

            while (filesAreTheSame == false)
            {
                lblTaskInfo.Text = "Connecting to database...";
                GUIrefresh();
                // Check if remote file exists:
                if (File.Exists(Path.Combine(excellRemoteFileLocation, excellFileName)))
                {
                    //Check if our Local file exists:
                    if (File.Exists(Path.Combine(excellLocalFileLocation, excellFileName)))
                    {
                        if (CheckIfFilesAreTheSame())
                        {
                            // Files match, launch it:
                            LaunchTheExcellFile();
                            ExitApp();
                        }
                        else
                        {
                            // File doesn't match. Downloading file from Remote server:
                            DownloadFileFromRemoteServer();
                        }
                    }
                    else
                    {
                        // File doesn't exist. Downloading file from Remote server:
                        DownloadFileFromRemoteServer();
                    }
                }
                else
                {
                    //MessageBox.Show("File: " + Path.Combine(excellRemoteFileLocation, excellFileName).ToString() + "\ncould not be found!\nContact the Administrator.");
                    MessageBox.Show("File: " + excellFileName + "\ncould not be found!\nContact the Administrator.", appName);
                    ExitApp();
                }
            }
        }

        private bool CheckIfFilesAreTheSame()
        {
            lblTaskInfo.Text = "Checking for new version...";
            GUIrefresh();
            byte[] fileLocal = null;
            byte[] fileRemote = null;

            try
            {
                fileLocal = File.ReadAllBytes(Path.Combine(excellLocalFileLocation, excellFileName));
                fileRemote = File.ReadAllBytes(Path.Combine(excellRemoteFileLocation, excellFileName));
            }
            catch (IOException)
            {
                // File might be used by another process, so copy it to local temp:
                File.Copy((Path.Combine(excellLocalFileLocation, excellFileName)), (Path.Combine(excellLocalFileLocation, excellFileName + ".tmp")), true);
                fileRemote = File.ReadAllBytes(Path.Combine(excellLocalFileLocation, excellFileName + ".tmp"));
            }
            

            if (fileLocal.Length == fileRemote.Length)
            {
                for (int i = 0; i < fileLocal.Length; i++)
                {
                    if (fileLocal[i] != fileRemote[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        private void LaunchTheExcellFile()
        {
            lblTaskInfo.Text = "Launching the file...";
            GUIrefresh();
            Process.Start(Path.Combine(excellLocalFileLocation, excellFileName));
        }

        private void DownloadFileFromRemoteServer()
        {
            lblTaskInfo.Text = "Downloading from remote server...";
            GUIrefresh();
            // Create dir if it doesn't exist on our pc:
            if (!Directory.Exists(excellLocalFileLocation))
            {
                Directory.CreateDirectory(excellLocalFileLocation);
            }
            File.Copy(Path.Combine(excellRemoteFileLocation, excellFileName), Path.Combine(excellLocalFileLocation, excellFileName), true);
        }

        private void DeleteTempFiles()
        {
            lblTaskInfo.Text = "Deleting temporary files...";
            GUIrefresh();
            if (File.Exists(Path.Combine(excellLocalFileLocation, excellFileName + ".tmp")))
            {
                File.Delete(Path.Combine(excellLocalFileLocation, excellFileName + ".tmp"));
            }
        }

        private void ExitApp()
        {
            DeleteTempFiles();
            Environment.Exit(1);
        } 
        
        private void GUIrefresh()
        {
            this.Refresh();
            //lblTaskInfo.Refresh();
            //txtAppName.Refresh();
            //pictureBox1.Refresh();
        }
    }
}
